from setuptools import setup

setup(
    name="voyager",
    version="0.2.0",
    py_modules=["voyager"],
    install_requires=["Click", "fuzzywuzzy[speedup]"],
    package_dir={"": "src"},
    entry_points={
        "console_scripts": [
            "voyager = voyager:cli",
        ],
    },
)
