import os
import click
import subprocess
from fuzzywuzzy import process

path = "/home/"

projects = {}


def clean_root(root):
    return "/".join(root.split("/")[-2:])


def prep(project_name):
    key = process.extractOne(project_name, [key for key in projects.keys()])[0]
    if key in projects:
        return ["docker-compose", "--file", projects[key]]
    else:
        return False


@click.group()
def cli():
    """Easily start and stop docker compose'd projects from anywhere with Python"""
    find()
    pass


def find():
    results = []
    for root, dirs, files in os.walk(path):
        if "docker-compose.yml" in files:
            key = clean_root(root)
            projects[key] = os.path.join(root, "docker-compose.yml")


@cli.command()
def ls():
    """List available docker compose projects"""
    click.echo("Projects Discovered:")
    for key in projects.keys():
        click.echo(f"\t * {key}")


@cli.command()
@click.argument("project_name")
@click.option("--app", default=None)
def start(project_name, app):
    """Start a docker compose project."""
    if command := prep(project_name):
        command.append("up")
        command.append("-d")
        if app:
            command.append(app)

        subprocess.run(command)
    else:
        click.echo(f"No project was found for {project_name}")
        for key in projects.keys():
            click.echo(f"\t * {key}")


@cli.command()
@click.argument("project_name")
@click.option("--app", default=None)
def stop(project_name, app):
    """Stop a docker compose project."""
    command = prep(project_name)
    command.append("rm")
    command.append("-sv")
    if app:
        command.append(app)

    subprocess.run(command)


@cli.command()
@click.argument("project_name")
@click.option("--app", default=None)
def restart(project_name, app):
    """Restart a docker compose project."""
    command = prep(project_name)
    command.append("restart")
    if app:
        command.append(app)

    subprocess.run(command)


@cli.command()
@click.argument("project_name")
@click.option("--app", default=None)
def logs(project_name, app):
    """View container output from a docker compose project."""
    command = prep(project_name)
    command.append("logs")
    if app:
        command.append(app)

    subprocess.run(command)
