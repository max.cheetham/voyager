import pytest
from voyager import cli
from click.testing import CliRunner


def test_voyager():
    runner = CliRunner()
    result = runner.invoke(cli)
    assert result.exit_code == 0


def test_voyager_start():
    runner = CliRunner()
    result = runner.invoke(cli, "start voyager")
    assert result.exit_code == 0


def test_voyager_ls():
    runner = CliRunner()
    result = runner.invoke(cli, "ls")
    assert result.exit_code == 0


def test_voyager_restart():
    runner = CliRunner()
    result = runner.invoke(cli, "restart voyager")
    print(result.output)
    assert result.exit_code == 0


def test_voyager_logs():
    runner = CliRunner()
    result = runner.invoke(cli, "logs voyager")
    print(result.output)
    assert result.exit_code == 0
