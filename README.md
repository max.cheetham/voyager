# Voyager
Easily start and stop docker compose'd projects from anywhere with Python
## Setup
run `poetry shell` then `poetry install` to prep the .venv

## Usage

after installation run `voyager --help` to see your options

### `voyager ls`
lists directories where `docker-compose` files that have been found

### `voyager start [project]`
calls `docker-compose up` in the named directory

### `voyager start [project] --app [app]`
calls `docker-compose up []` in the named directory

### `voyager stop [project]`
calls `docker-compose rm -sv` in the named directory

find_near_matches('Max Cheetham', 'mx cheetham', max_deletions=1, max_insertions=1, max_substitutions=0)